<?php

namespace App\Http\Controllers;

use InfyOm\Generator\Utils\ResponseUtil;
use Response;

    /**
    *   @SWG\Info(
    *     title="Laravel Generator APIs",
    *     version="1.0.0",
    *   )
    * @SWG\Swagger(
    *     
    *     schemes={"http", "https"},
    *       
    *   
    * @SWG\SecurityScheme(
    *     securityDefinition="Bearer",
    *     type="apiKey",
    *     name="Authorization",
    *     in="header",
    * )
    * )
    * 
    */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }
}
